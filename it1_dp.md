# Iteración 1: Diseño y planificación

## Diagrama de clases 
![Captura1](/uploads/a195ac2f1079e493ed5e9e5c2d4f1823/Captura1.png)


## Backlog de la iteración

1. Como administrador, quiero registrar ingresos nuevos de insumos.
2. Como administrador, quiero poder gestionar los proveedores de mis insumos.
3. Como administrador, quiero poder gestionar productos y sus categorías.